#!/bin/bash -e

HOST=$1

cargo deb --target=arm-unknown-linux-gnueabihf
scp target/arm-unknown-linux-gnueabihf/debian/mq_to_postgresql_0.1.0_armhf.deb  pi@${HOST}:
ssh pi@${HOST} "sudo dpkg -i mq_to_postgresql_0.1.0_armhf.deb"
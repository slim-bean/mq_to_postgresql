
Make sure the cross compiler is in the path, needed for some libs like log4rs

```shell
export PATH=$PATH:/home/user/cc/gcc-linaro-7.3.1-2018.05-x86_64_arm-linux-gnueabihf/bin/
```

To connect to the pi database, tunnel postgres over ssh:

```shell
ssh -L 5432:localhost:5432 pi@172.20.30.60
```
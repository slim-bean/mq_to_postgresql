#[macro_use]
extern crate log;
extern crate log4rs;
extern crate mosquitto_client;
extern crate serde_json;
extern crate serde_yaml;
extern crate r2d2;
extern crate r2d2_postgres;
extern crate postgres;


extern crate sensor_lib;

use sensor_lib::{SensorValue, TempHumidityValue, LightValue, WindSpeedDirValue, AirParticulateValue, ElectricValue, ThermostatValue, HeartRateValue};
use r2d2_postgres::{TlsMode, PostgresConnectionManager};

extern crate chrono;
use chrono::{TimeZone, Local};

use std::path::Path;



fn main() {

    //Check if there is a logging config configured in /etc if not just use one local to the app
    let etc_config = Path::new("/etc/mq_to_postgresql/log4rs.yml");
    let log_config_path = if let true = etc_config.exists() {
        etc_config
    } else {
        Path::new("log4rs.yml")
    };
    log4rs::init_file(log_config_path, Default::default()).expect("Failed to init logger");

    info!("Starting mq_to_prometheus");

    let manager = PostgresConnectionManager::new("postgres://loader:hoagie@localhost/sensors",
                                                 TlsMode::None).unwrap();
    let pool = r2d2::Pool::builder()
        .max_size(2)
        .build(manager)
        .unwrap();


    let m = mosquitto_client::Mosquitto::new("mq_to_postgresql");
    m.connect_wait("localhost", 1883, 5000, Some(60)).expect("Failed to connect");

    info!("Connected");

    let generic_sub = m.subscribe("/ws/+/grp/generic", 2).expect("can't subscribe to /ws/+/generic");
    let light_sub = m.subscribe("/ws/+/grp/light", 2).expect("can't subscribe to /ws/+/light");
    let temp_humidity_sub = m.subscribe("/ws/+/grp/temp_humidity", 2).expect("can't subscribe to /ws/+/temp_humidity");
    let wind_speed_dir_sub = m.subscribe("/ws/+/grp/wind_speed_dir", 2).expect("can't subscribe to /ws/+/temp_humidity");
    let air_particulate_sub = m.subscribe("/ws/+/grp/air_particulate", 2).expect("can't subscribe to /ws/+/air_particulate");
    let electric_sub = m.subscribe("/ws/+/grp/electric", 2).expect("can't subscribe to /ws/+/electric");
    let thermostat_sub = m.subscribe("/ws/+/grp/thermostat", 2).expect("can't subscribe to /ws/+/thermostat");
    let heartrate_sub = m.subscribe("/ws/+/grp/heartrate", 2).expect("can't subscribe to /ws/+/heartrate");



    let mut mc = m.callbacks(pool);
    mc.on_message(|pool, msg| {
        if generic_sub.matches(&msg) {
            match serde_json::from_slice::<SensorValue>(msg.payload()) {
                Ok(val) => {
                    info!("Received {:?}", val);
                    let conn = pool.get().unwrap();
                    match val.value.parse::<f32>() {
                        Ok(parsed_float) => {
                            conn.execute("INSERT INTO generic_data  (time, sensor_id, value) VALUES ($1, $2, $3)",
                                         &[&Local.timestamp_millis(val.timestamp as i64), &val.id, &parsed_float])
                                .unwrap_or_else(|err|{
                                    error!("Failed to insert generic sensor value into database with error: {}", err);
                                    0 //execute returns the number of rows, if there was an error 0 rows were modified
                                });
                        },
                        Err(err) => {
                            error!("Failed to parse generic sensors' value to float, val: `{}`, err: {}", val.value, err);
                        },
                    }

                }
                Err(err) => {
                    error!("Failed to deserialize SensorValue with error: {}", err);
                }
            };
        } else if temp_humidity_sub.matches(&msg) {
            match serde_json::from_slice::<TempHumidityValue>(msg.payload()) {
                Ok(val) => {
                    info!("Received {:?}", val);
                    let conn = pool.get().unwrap();
                    conn.execute("INSERT INTO temp_humidity  (time, location, temperature, humidity) VALUES ($1, $2, $3, $4)",
                                 &[&Local.timestamp_millis(val.timestamp as i64), &val.location, &val.temp, &val.humidity])
                        .unwrap_or_else(|err|{
                            error!("Failed to insert temp_humidity value into database with error: {}", err);
                            0 //execute returns the number of rows, if there was an error 0 rows were modified
                        });
                }
                Err(err) => {
                    error!("Failed to deserialize TempHumidityValue with error: {}", err);
                }
            };
        } else if light_sub.matches(&msg) {
            match serde_json::from_slice::<LightValue>(msg.payload()) {
                Ok(val) => {
                    info!("Received {:?}", val);
                    let conn = pool.get().unwrap();
                    conn.execute("INSERT INTO outside_light (time, location, uv_raw, uv_index, vis_raw, ir_raw, lux) VALUES ($1, $2, $3, $4, $5, $6, $7)",
                                 &[&Local.timestamp_millis(val.timestamp as i64), &val.location, &val.uv_raw, &val.uv_index, &val.vis_raw, &val.ir_raw, &val.lux])
                        .unwrap_or_else(|err|{
                            error!("Failed to insert outside_light value into database with error: {}", err);
                            0 //execute returns the number of rows, if there was an error 0 rows were modified
                        });
                }
                Err(err) => {
                    error!("Failed to deserialize LightValue with error: {}", err);
                }
            };
        } else if wind_speed_dir_sub.matches(&msg) {
            match serde_json::from_slice::<WindSpeedDirValue>(msg.payload()) {
                Ok(val) => {
                    //info!("Received {:?}", val);
                    let conn = pool.get().unwrap();
                    conn.execute("INSERT INTO wind_speed_dir (time, location, speed, dir) VALUES ($1, $2, $3, $4)",
                                 &[&Local.timestamp_millis(val.timestamp as i64), &val.location, &val.speed, &val.dir])
                        .unwrap_or_else(|err|{
                            error!("Failed to insert wind_speed_dir value into database with error: {}", err);
                            0 //execute returns the number of rows, if there was an error 0 rows were modified
                        });
                }
                Err(err) => {
                    error!("Failed to deserialize LightValue with error: {}", err);
                }
            };
        } else if air_particulate_sub.matches(&msg) {
            match serde_json::from_slice::<AirParticulateValue>(msg.payload()) {
                Ok(val) => {
                    //info!("Received {:?}", val);
                    let conn = pool.get().unwrap();
                    conn.execute("INSERT INTO air_particulate (time, location, pm2_5, pm10) VALUES ($1, $2, $3, $4)",
                                 &[&Local.timestamp_millis(val.timestamp as i64), &val.location, &val.pm2_5, &val.pm10])
                        .unwrap_or_else(|err|{
                            error!("Failed to insert air_particulate value into database with error: {}", err);
                            0 //execute returns the number of rows, if there was an error 0 rows were modified
                        });
                }
                Err(err) => {
                    error!("Failed to deserialize LightValue with error: {}", err);
                }
            };
        } else if electric_sub.matches(&msg) {
            match serde_json::from_slice::<ElectricValue>(msg.payload()) {
                Ok(val) => {
                    //info!("Received {:?}", val);
                    let conn = pool.get().unwrap();
                    conn.execute("INSERT INTO electric (time, location, total_kwh, total_reactive, total_reverse, volts_l1, volts_l2, amps_l1, amps_l2, watts_l1, watts_l2, watts_total, pf_l1, pf_l2, reactive_l1, reactive_l2, reactive_total, frequency) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14, $15, $16, $17, $18)",
                                 &[&Local.timestamp_millis(val.timestamp as i64), &val.location, &val.total_kwh, &val.total_reactive, &val.total_reverse, &val.volts_l1, &val.volts_l2, &val.amps_l1, &val.amps_l2, &val.watts_l1, &val.watts_l2, &val.watts_total, &val.pf_l1, &val.pf_l2, &val.reactive_l1, &val.reactive_l2, &val.reactive_total, &val.frequency])
                        .unwrap_or_else(|err|{
                            error!("Failed to insert electric value into database with error: {}", err);
                            0 //execute returns the number of rows, if there was an error 0 rows were modified
                        });
                }
                Err(err) => {
                    error!("Failed to deserialize ElectricValue with error: {}", err);
                }
            };
        } else if thermostat_sub.matches(&msg) {
            match serde_json::from_slice::<ThermostatValue>(msg.payload()) {
                Ok(val) => {
                    info!("Received {:?}", val);
                    let conn = pool.get().unwrap();
                    conn.execute("INSERT INTO thermostat (time, temp, tmode, fmode, t_heat, t_cool, tstate, fstate) VALUES ($1, $2, $3, $4, $5, $6, $7, $8)",
                                 &[&Local.timestamp_millis(val.timestamp as i64), &val.temp, &val.tmode, &val.fmode, &val.t_heat, &val.t_cool, &val.tstate, &val.fstate])
                        .unwrap_or_else(|err|{
                            error!("Failed to insert thermostat value into database with error: {}", err);
                            0 //execute returns the number of rows, if there was an error 0 rows were modified
                        });
                }
                Err(err) => {
                    error!("Failed to deserialize ThermostatValue with error: {}", err);
                }
            };
        } else if heartrate_sub.matches(&msg) {
            match serde_json::from_slice::<HeartRateValue>(msg.payload()) {
                Ok(val) => {
                    info!("Received {:?}", val);
                    let conn = pool.get().unwrap();
                    conn.execute("INSERT INTO heartrate (time, user_id, rate) VALUES ($1, $2, $3)",
                                 &[&Local.timestamp_millis(val.timestamp as i64), &val.user_id, &val.rate])
                        .unwrap_or_else(|err|{
                            error!("Failed to insert heartrate value into database with error: {}", err);
                            0 //execute returns the number of rows, if there was an error 0 rows were modified
                        });
                }
                Err(err) => {
                    error!("Failed to deserialize ThermostatValue with error: {}", err);
                }
            };
        } else {
            error!("UnMatched Message: {:?}", msg.payload());
        }
    });


    mc.on_subscribe(|_, id| {
        info!("Subscribed: {}", id);
    });

    loop {
        m.do_loop(300).unwrap();
    }
}


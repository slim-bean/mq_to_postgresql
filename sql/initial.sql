

CREATE TABLE generic_data (
  time        TIMESTAMPTZ       NOT NULL,
  sensor_id   SMALLINT          NOT NULL,
  value       REAL              NULL
);

SELECT create_hypertable('generic_data', 'time');

CREATE TABLE temp_humidity (
  time        TIMESTAMPTZ       NOT NULL,
  location    SMALLINT          NOT NULL,
  temperature REAL              NULL,
  humidity    REAL              NULL
);

SELECT create_hypertable('temp_humidity', 'time');

CREATE TABLE outside_light (
  time        TIMESTAMPTZ       NOT NULL,
  location    SMALLINT          NOT NULL,
  uv_raw      INTEGER           NULL,
  uv_index    REAL              NULL,
  vis_raw     INTEGER           NULL,
  ir_raw      INTEGER           NULL,
  lux         INTEGER           NULL
);

SELECT create_hypertable('outside_light', 'time');

CREATE TABLE wind_speed_dir (
  time        TIMESTAMPTZ       NOT NULL,
  location    SMALLINT          NOT NULL,
  speed       SMALLINT          NULL,
  dir         SMALLINT          NULL
);

SELECT create_hypertable('wind_speed_dir', 'time');

CREATE TABLE air_particulate (
  time        TIMESTAMPTZ       NOT NULL,
  location    SMALLINT          NOT NULL,
  pm2_5       SMALLINT          NULL,
  pm10        SMALLINT          NULL
);

SELECT create_hypertable('air_particulate', 'time');


CREATE TABLE electric (
  time            TIMESTAMPTZ       NOT NULL,
  location        SMALLINT          NOT NULL,
  total_kwh       DOUBLE PRECISION  NULL,
  total_reactive  DOUBLE PRECISION  NULL,
  total_reverse   DOUBLE PRECISION  NULL,
  volts_l1        REAL              NULL,
  volts_l2        REAL              NULL,
  amps_l1         REAL              NULL,
  amps_l2         REAL              NULL,
  watts_l1        SMALLINT          NULL,
  watts_l2        SMALLINT          NULL,
  watts_total     SMALLINT          NULL,
  pf_l1           REAL              NULL,
  pf_l2           REAL              NULL,
  reactive_l1     SMALLINT          NULL,
  reactive_l2     SMALLINT          NULL,
  reactive_total  SMALLINT          NULL,
  frequency       REAL              NULL
);

SELECT create_hypertable('electric', 'time');

CREATE TABLE propane_fill (
  fill_date    TIMESTAMPTZ     NOT NULL,
  gallons      REAL            NULL
);

CREATE TABLE propane_price (
  date        TIMESTAMPTZ     NOT NULL,
  price       NUMERIC(6,2)     NOT NULL
);

CREATE TABLE thermostat (
  time      TIMESTAMPTZ   NOT NULL,
  temp      REAL          NULL,
  tmode     SMALLINT      NULL,
  fmode     SMALLINT      NULL,
  t_heat    REAL          NULL,
  t_cool    REAL          NULL,
  tstate    SMALLINT      NULL,
  fstate    SMALLINT      NULL
);

SELECT create_hypertable('thermostat', 'time');

CREATE TABLE heartrate (
  time      TIMESTAMPTZ   NOT NULL,
  user_id   SMALLINT      NOT NULL,
  rate      SMALLINT      NOT NULL
);

SELECT create_hypertable('heartrate', 'time');

CREATE USER loader WITH PASSWORD 'hoagie' LOGIN;
CREATE USER grafana WITH PASSWORD 'submarine' LOGIN;

GRANT SELECT, INSERT ON ALL TABLES IN SCHEMA public TO loader;
GRANT SELECT ON ALL TABLES IN SCHEMA public to grafana;


